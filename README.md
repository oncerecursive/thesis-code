# Master's Thesis code & data repository

The .csv-files are in a more readable format here:
https://docs.google.com/spreadsheets/d/1M5dvq6ScsYslIWdyfM2V4slYmbzMIiA-HNl_WbpsSv8/edit?usp=sharing

The sensitivity calculations for determining the r-value in MST+r networks can
be found on the first sheet, and on the second sheet are the results for
all networks created using norm=1, r=0.2 and varying matching matrices
and regions.